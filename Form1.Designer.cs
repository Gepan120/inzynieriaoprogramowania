
namespace UsługiLotnicze
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.flaightsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.airflyDataSet = new UsługiLotnicze.AirflyDataSet();
            this.flaightsTableAdapter = new UsługiLotnicze.AirflyDataSetTableAdapters.FlaightsTableAdapter();
            this.tableAdapterManager = new UsługiLotnicze.AirflyDataSetTableAdapters.TableAdapterManager();
            this.accountsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.accountsTableAdapter = new UsługiLotnicze.AirflyDataSetTableAdapters.AccountsTableAdapter();
            this.rezerwationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rezerwationsTableAdapter = new UsługiLotnicze.AirflyDataSetTableAdapters.rezerwationsTableAdapter();
            this.flaightsDataGridView = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startingplaceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.destinationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.miejscaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cenaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataOdlotuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sptextbox = new System.Windows.Forms.TextBox();
            this.destextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.serchbutton = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.flaightsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.airflyDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rezerwationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flaightsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(734, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Zaloguj się";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(642, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Zarejestruj się";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // flaightsBindingSource
            // 
            this.flaightsBindingSource.DataMember = "Flaights";
            this.flaightsBindingSource.DataSource = this.airflyDataSet;
            // 
            // airflyDataSet
            // 
            this.airflyDataSet.DataSetName = "AirflyDataSet";
            this.airflyDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // flaightsTableAdapter
            // 
            this.flaightsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AccountsTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.FlaightsTableAdapter = this.flaightsTableAdapter;
            this.tableAdapterManager.rezerwationsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = UsługiLotnicze.AirflyDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // accountsBindingSource
            // 
            this.accountsBindingSource.DataMember = "Accounts";
            this.accountsBindingSource.DataSource = this.airflyDataSet;
            // 
            // accountsTableAdapter
            // 
            this.accountsTableAdapter.ClearBeforeFill = true;
            // 
            // rezerwationsBindingSource
            // 
            this.rezerwationsBindingSource.DataMember = "rezerwations";
            this.rezerwationsBindingSource.DataSource = this.airflyDataSet;
            // 
            // rezerwationsTableAdapter
            // 
            this.rezerwationsTableAdapter.ClearBeforeFill = true;
            // 
            // flaightsDataGridView
            // 
            this.flaightsDataGridView.AutoGenerateColumns = false;
            this.flaightsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.flaightsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.startingplaceDataGridViewTextBoxColumn,
            this.destinationDataGridViewTextBoxColumn,
            this.miejscaDataGridViewTextBoxColumn,
            this.cenaDataGridViewTextBoxColumn,
            this.dataOdlotuDataGridViewTextBoxColumn});
            this.flaightsDataGridView.DataSource = this.flaightsBindingSource;
            this.flaightsDataGridView.Location = new System.Drawing.Point(166, 110);
            this.flaightsDataGridView.Name = "flaightsDataGridView";
            this.flaightsDataGridView.Size = new System.Drawing.Size(643, 337);
            this.flaightsDataGridView.TabIndex = 2;
            this.flaightsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.flaightsDataGridView_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // startingplaceDataGridViewTextBoxColumn
            // 
            this.startingplaceDataGridViewTextBoxColumn.DataPropertyName = "startingplace";
            this.startingplaceDataGridViewTextBoxColumn.HeaderText = "startingplace";
            this.startingplaceDataGridViewTextBoxColumn.Name = "startingplaceDataGridViewTextBoxColumn";
            // 
            // destinationDataGridViewTextBoxColumn
            // 
            this.destinationDataGridViewTextBoxColumn.DataPropertyName = "destination";
            this.destinationDataGridViewTextBoxColumn.HeaderText = "destination";
            this.destinationDataGridViewTextBoxColumn.Name = "destinationDataGridViewTextBoxColumn";
            // 
            // miejscaDataGridViewTextBoxColumn
            // 
            this.miejscaDataGridViewTextBoxColumn.DataPropertyName = "miejsca";
            this.miejscaDataGridViewTextBoxColumn.HeaderText = "miejsca";
            this.miejscaDataGridViewTextBoxColumn.Name = "miejscaDataGridViewTextBoxColumn";
            // 
            // cenaDataGridViewTextBoxColumn
            // 
            this.cenaDataGridViewTextBoxColumn.DataPropertyName = "cena";
            this.cenaDataGridViewTextBoxColumn.HeaderText = "cena";
            this.cenaDataGridViewTextBoxColumn.Name = "cenaDataGridViewTextBoxColumn";
            // 
            // dataOdlotuDataGridViewTextBoxColumn
            // 
            this.dataOdlotuDataGridViewTextBoxColumn.DataPropertyName = "Data odlotu";
            this.dataOdlotuDataGridViewTextBoxColumn.HeaderText = "Data odlotu";
            this.dataOdlotuDataGridViewTextBoxColumn.Name = "dataOdlotuDataGridViewTextBoxColumn";
            // 
            // sptextbox
            // 
            this.sptextbox.Location = new System.Drawing.Point(93, 66);
            this.sptextbox.Name = "sptextbox";
            this.sptextbox.Size = new System.Drawing.Size(100, 20);
            this.sptextbox.TabIndex = 3;
            this.sptextbox.TextChanged += new System.EventHandler(this.sptextbox_TextChanged);
            // 
            // destextbox
            // 
            this.destextbox.Location = new System.Drawing.Point(300, 65);
            this.destextbox.Name = "destextbox";
            this.destextbox.Size = new System.Drawing.Size(100, 20);
            this.destextbox.TabIndex = 4;
            this.destextbox.TextChanged += new System.EventHandler(this.destextbox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Miejsce startu:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(199, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Miejsce docelowe:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(406, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Data odlotu:";
            // 
            // serchbutton
            // 
            this.serchbutton.Location = new System.Drawing.Point(722, 64);
            this.serchbutton.Name = "serchbutton";
            this.serchbutton.Size = new System.Drawing.Size(75, 23);
            this.serchbutton.TabIndex = 9;
            this.serchbutton.Text = "Szukaj";
            this.serchbutton.UseVisualStyleBackColor = true;
            this.serchbutton.Click += new System.EventHandler(this.serchbutton_Click_1);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(477, 66);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 469);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.serchbutton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.destextbox);
            this.Controls.Add(this.sptextbox);
            this.Controls.Add(this.flaightsDataGridView);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flaightsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.airflyDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rezerwationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flaightsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private AirflyDataSet airflyDataSet;
        private System.Windows.Forms.BindingSource flaightsBindingSource;
        private AirflyDataSetTableAdapters.FlaightsTableAdapter flaightsTableAdapter;
        private AirflyDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.BindingSource accountsBindingSource;
        private AirflyDataSetTableAdapters.AccountsTableAdapter accountsTableAdapter;
        private System.Windows.Forms.BindingSource rezerwationsBindingSource;
        private AirflyDataSetTableAdapters.rezerwationsTableAdapter rezerwationsTableAdapter;
        private System.Windows.Forms.DataGridView flaightsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startingplaceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn destinationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn miejscaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cenaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataOdlotuDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox sptextbox;
        private System.Windows.Forms.TextBox destextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button serchbutton;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}

