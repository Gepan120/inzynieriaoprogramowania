var annotated_dup =
[
    [ "UsługiLotnicze", "namespace_us_xC5_x82ugi_lotnicze.html", [
      [ "AirflyDataSetTableAdapters", "namespace_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters.html", [
        [ "AccountsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_accounts_table_adapter.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_accounts_table_adapter" ],
        [ "rezerwationsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter" ],
        [ "FlaightsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter" ],
        [ "TableAdapterManager", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager" ]
      ] ],
      [ "AirflyDataSet", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set" ],
      [ "AirflyDataSet1", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set1.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set1" ],
      [ "Form1", "class_us_xC5_x82ugi_lotnicze_1_1_form1.html", "class_us_xC5_x82ugi_lotnicze_1_1_form1" ],
      [ "Form2", "class_us_xC5_x82ugi_lotnicze_1_1_form2.html", "class_us_xC5_x82ugi_lotnicze_1_1_form2" ],
      [ "Form3", "class_us_xC5_x82ugi_lotnicze_1_1_form3.html", "class_us_xC5_x82ugi_lotnicze_1_1_form3" ],
      [ "Form4", "class_us_xC5_x82ugi_lotnicze_1_1_form4.html", "class_us_xC5_x82ugi_lotnicze_1_1_form4" ],
      [ "Form5", "class_us_xC5_x82ugi_lotnicze_1_1_form5.html", "class_us_xC5_x82ugi_lotnicze_1_1_form5" ]
    ] ]
];