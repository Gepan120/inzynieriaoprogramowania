var class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter =
[
    [ "FlaightsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#aaeda94759ea607eb8544be05b972ac1d", null ],
    [ "Delete", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a3d48afd65d9c418baf8444955c9f758d", null ],
    [ "Fill", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#ad2ad65c115e1f17b51fdc40c14d62f12", null ],
    [ "FillBy", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a7de975de8367a3e7f5d54565c59989b4", null ],
    [ "GetData", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a7285770433a267301e8d523faa3b7305", null ],
    [ "Insert", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a3e8db724f2d64e1b001f47e9e97a0317", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a1fac74fe5bd1e72d5d880bcdadc214bf", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a350c596524feeadb5b12cf6092086b27", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#afebeb884e15a2d760367a84e7c4ebd4f", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a8da62daccf603097322e88003758fb8c", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a84ab9e97600abdc8c25e0f8365c5c778", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a886ce061467da93ecad0e9d5afa9c80e", null ],
    [ "Adapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a9651eafcb72f00fad2279896ec80cbd2", null ],
    [ "ClearBeforeFill", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a729ef278cd05aeafb9295582fa49c5ea", null ],
    [ "CommandCollection", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#affb68e19cc1915aa9c634b1191672aab", null ],
    [ "Connection", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#ad24cfb073a102eedca858c91fc98a3bb", null ],
    [ "Transaction", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html#a909417721c34db73a9bbcfd13f3e7360", null ]
];