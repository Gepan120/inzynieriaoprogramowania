var files_dup =
[
    [ "AirflyDataSet.cs", "_airfly_data_set_8cs.html", [
      [ "AirflyDataSet", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set" ],
      [ "AccountsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_accounts_table_adapter.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_accounts_table_adapter" ],
      [ "rezerwationsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter" ],
      [ "FlaightsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter" ]
    ] ],
    [ "AirflyDataSet.Designer.cs", "_airfly_data_set_8_designer_8cs.html", [
      [ "AirflyDataSet", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set" ],
      [ "AccountsDataTable", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_accounts_data_table.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_accounts_data_table" ],
      [ "FlaightsDataTable", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_flaights_data_table.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_flaights_data_table" ],
      [ "rezerwationsDataTable", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1rezerwations_data_table.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1rezerwations_data_table" ],
      [ "AccountsRow", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_accounts_row.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_accounts_row" ],
      [ "FlaightsRow", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_flaights_row.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_flaights_row" ],
      [ "rezerwationsRow", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1rezerwations_row.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1rezerwations_row" ],
      [ "AccountsRowChangeEvent", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_accounts_row_change_event.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_accounts_row_change_event" ],
      [ "FlaightsRowChangeEvent", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_flaights_row_change_event.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_flaights_row_change_event" ],
      [ "rezerwationsRowChangeEvent", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1rezerwations_row_change_event.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1rezerwations_row_change_event" ],
      [ "AccountsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_accounts_table_adapter.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_accounts_table_adapter" ],
      [ "FlaightsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter" ],
      [ "rezerwationsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter" ],
      [ "TableAdapterManager", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager" ]
    ] ],
    [ "AirflyDataSet1.Designer.cs", "_airfly_data_set1_8_designer_8cs.html", [
      [ "AirflyDataSet1", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set1.html", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set1" ]
    ] ],
    [ "Form1.cs", "_form1_8cs.html", [
      [ "Form1", "class_us_xC5_x82ugi_lotnicze_1_1_form1.html", "class_us_xC5_x82ugi_lotnicze_1_1_form1" ]
    ] ],
    [ "Form1.Designer.cs", "_form1_8_designer_8cs.html", [
      [ "Form1", "class_us_xC5_x82ugi_lotnicze_1_1_form1.html", "class_us_xC5_x82ugi_lotnicze_1_1_form1" ]
    ] ],
    [ "Form2.cs", "_form2_8cs.html", [
      [ "Form2", "class_us_xC5_x82ugi_lotnicze_1_1_form2.html", "class_us_xC5_x82ugi_lotnicze_1_1_form2" ]
    ] ],
    [ "Form2.Designer.cs", "_form2_8_designer_8cs.html", [
      [ "Form2", "class_us_xC5_x82ugi_lotnicze_1_1_form2.html", "class_us_xC5_x82ugi_lotnicze_1_1_form2" ]
    ] ],
    [ "Form3.cs", "_form3_8cs.html", [
      [ "Form3", "class_us_xC5_x82ugi_lotnicze_1_1_form3.html", "class_us_xC5_x82ugi_lotnicze_1_1_form3" ]
    ] ],
    [ "Form3.Designer.cs", "_form3_8_designer_8cs.html", [
      [ "Form3", "class_us_xC5_x82ugi_lotnicze_1_1_form3.html", "class_us_xC5_x82ugi_lotnicze_1_1_form3" ]
    ] ],
    [ "Form4.cs", "_form4_8cs.html", [
      [ "Form4", "class_us_xC5_x82ugi_lotnicze_1_1_form4.html", "class_us_xC5_x82ugi_lotnicze_1_1_form4" ]
    ] ],
    [ "Form4.Designer.cs", "_form4_8_designer_8cs.html", [
      [ "Form4", "class_us_xC5_x82ugi_lotnicze_1_1_form4.html", "class_us_xC5_x82ugi_lotnicze_1_1_form4" ]
    ] ],
    [ "Form5.cs", "_form5_8cs.html", [
      [ "Form5", "class_us_xC5_x82ugi_lotnicze_1_1_form5.html", "class_us_xC5_x82ugi_lotnicze_1_1_form5" ]
    ] ],
    [ "Form5.Designer.cs", "_form5_8_designer_8cs.html", [
      [ "Form5", "class_us_xC5_x82ugi_lotnicze_1_1_form5.html", "class_us_xC5_x82ugi_lotnicze_1_1_form5" ]
    ] ],
    [ "Program.cs", "_program_8cs.html", null ]
];