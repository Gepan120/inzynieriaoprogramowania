var hierarchy =
[
    [ "global.SystemComponentModel.Component", null, [
      [ "UsługiLotnicze.AirflyDataSetTableAdapters.AccountsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_accounts_table_adapter.html", null ],
      [ "UsługiLotnicze.AirflyDataSetTableAdapters.FlaightsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_flaights_table_adapter.html", null ],
      [ "UsługiLotnicze.AirflyDataSetTableAdapters.TableAdapterManager", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html", null ],
      [ "UsługiLotnicze.AirflyDataSetTableAdapters.rezerwationsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html", null ]
    ] ],
    [ "global.SystemData.DataRow", null, [
      [ "UsługiLotnicze.AirflyDataSet.AccountsRow", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_accounts_row.html", null ],
      [ "UsługiLotnicze.AirflyDataSet.FlaightsRow", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_flaights_row.html", null ],
      [ "UsługiLotnicze.AirflyDataSet.rezerwationsRow", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1rezerwations_row.html", null ]
    ] ],
    [ "global.SystemData.DataSet", null, [
      [ "UsługiLotnicze.AirflyDataSet", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set.html", null ],
      [ "UsługiLotnicze.AirflyDataSet1", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set1.html", null ]
    ] ],
    [ "Form", null, [
      [ "UsługiLotnicze.Form1", "class_us_xC5_x82ugi_lotnicze_1_1_form1.html", null ],
      [ "UsługiLotnicze.Form2", "class_us_xC5_x82ugi_lotnicze_1_1_form2.html", null ],
      [ "UsługiLotnicze.Form3", "class_us_xC5_x82ugi_lotnicze_1_1_form3.html", null ],
      [ "UsługiLotnicze.Form4", "class_us_xC5_x82ugi_lotnicze_1_1_form4.html", null ],
      [ "UsługiLotnicze.Form5", "class_us_xC5_x82ugi_lotnicze_1_1_form5.html", null ]
    ] ],
    [ "global.SystemEventArgs", null, [
      [ "UsługiLotnicze.AirflyDataSet.AccountsRowChangeEvent", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_accounts_row_change_event.html", null ],
      [ "UsługiLotnicze.AirflyDataSet.FlaightsRowChangeEvent", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_flaights_row_change_event.html", null ],
      [ "UsługiLotnicze.AirflyDataSet.rezerwationsRowChangeEvent", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1rezerwations_row_change_event.html", null ]
    ] ],
    [ "global.SystemData.TypedTableBase", null, [
      [ "UsługiLotnicze.AirflyDataSet.AccountsDataTable", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_accounts_data_table.html", null ],
      [ "UsługiLotnicze.AirflyDataSet.FlaightsDataTable", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1_flaights_data_table.html", null ],
      [ "UsługiLotnicze.AirflyDataSet.rezerwationsDataTable", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_1_1rezerwations_data_table.html", null ]
    ] ]
];