var indexSectionsWithContent =
{
  0: "abcdefgiklmnoprstu",
  1: "afrt",
  2: "u",
  3: "afp",
  4: "acdfgimnorsu",
  5: "adil",
  6: "u",
  7: "iu",
  8: "abcdefiklmnprstu",
  9: "afr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events"
};

var indexSectionLabels =
{
  0: "Wszystko",
  1: "Klasy",
  2: "Przestrzenie nazw",
  3: "Pliki",
  4: "Funkcje",
  5: "Zmienne",
  6: "Wyliczenia",
  7: "Wartości wyliczeń",
  8: "Właściwości",
  9: "Zdarzenia"
};

