var class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager =
[
    [ "UpdateOrderOption", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#afe8fcc750e5c7a6bc4b175e0956efe05", [
      [ "InsertUpdateDelete", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#afe8fcc750e5c7a6bc4b175e0956efe05a27b77cb15d3da7ded0250d0001bc6755", null ],
      [ "UpdateInsertDelete", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#afe8fcc750e5c7a6bc4b175e0956efe05a894fcc001e51f673d3fb5f3096473dd8", null ]
    ] ],
    [ "MatchTableAdapterConnection", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#a3f5ad25f6153c8bd9edcad925a2e194c", null ],
    [ "SortSelfReferenceRows", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#ac0aa0f4bfcf29e527e2b50f33c4765dd", null ],
    [ "UpdateAll", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#ad7917bb1f1e39aaddfea1c6b92d2a1a2", null ],
    [ "AccountsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#a62f1fe7b099f379093c23295eb850436", null ],
    [ "BackupDataSetBeforeUpdate", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#af4c8a8e7324f45993f82d85a3c18c24a", null ],
    [ "Connection", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#a3f6ec8467176dba534f2bc5efe830345", null ],
    [ "FlaightsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#ac2f05d25cfd495bd6813b1e2b7de8cf9", null ],
    [ "rezerwationsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#a96dec6fa35d831d5d8c9b09e378a74da", null ],
    [ "TableAdapterInstanceCount", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#a6370fc2f32af45aefff63893b62687c7", null ],
    [ "UpdateOrder", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1_table_adapter_manager.html#af1031791f975e9329d855e6daff3d6d4", null ]
];