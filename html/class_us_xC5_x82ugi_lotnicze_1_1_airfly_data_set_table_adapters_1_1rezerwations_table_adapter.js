var class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter =
[
    [ "rezerwationsTableAdapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#a4679865338bde64406d642defa4154d2", null ],
    [ "Delete", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#a0fd5777450026c48b74f12589504bc67", null ],
    [ "Fill", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#a070fb0beaef92390b98e545ac9f85352", null ],
    [ "GetData", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#af06e6d1162e493513a610e7965716b2b", null ],
    [ "Insert", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#ae21ce99a39b5fe8c72380711222fe4c2", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#ab88f371a52b9ac611ca05906842380a6", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#a71b3f41cb5165b0ec5aba1e1c59ea19e", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#a68aa14142fc8dee702d6293696df047d", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#afd4ce05ab420979bce842ff8f8f55b78", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#a0eaab027b6639129f4c525c530e3d9ed", null ],
    [ "Update", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#a260cd95fd204170e705a04e61e1f239d", null ],
    [ "Adapter", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#a8e817323b6e992a9c4d2e339d07e1010", null ],
    [ "ClearBeforeFill", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#a4d7f979562549d82f9ab7e51c44a826d", null ],
    [ "CommandCollection", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#ae473f4d89b05500aaa6f69adb2969ab4", null ],
    [ "Connection", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#aece27f1853506a57ccd0c628c6aa8e47", null ],
    [ "Transaction", "class_us_xC5_x82ugi_lotnicze_1_1_airfly_data_set_table_adapters_1_1rezerwations_table_adapter.html#ae4325af767f7543806aff335b1b2ce7a", null ]
];