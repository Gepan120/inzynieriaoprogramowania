﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace UsługiLotnicze
{
    public partial class Form4 : Form
    {
        public int rezid, userid;
        public Form4(int id, int usrid)
        {
            InitializeComponent();
            rezid = id;
            userid = usrid;
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            /**
 * Podczas logowanie pobiera dane z konstruktora formularza wyświetlając wybrany lot, oraz uzupełniając imię i nazwisko zalogowanego użytkownika.
 */
            // TODO: Ten wiersz kodu wczytuje dane do tabeli 'airflyDataSet.Flaights' . Możesz go przenieść lub usunąć.
            this.flaightsTableAdapter.Fill(this.airflyDataSet.Flaights);
            var id = rezid;
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqldspl = new SqlDataAdapter();
            sqldspl.SelectCommand = new SqlCommand("Select * from Flaights where Id='" + id + "';", sc);
            DataTable dg = new DataTable();
            sqldspl.Fill(dg);
            dataGridView1.DataSource = dg;
            if (userid != 0)
            {
                SqlDataAdapter sqlusr = new SqlDataAdapter();
                sqlusr.SelectCommand = new SqlCommand("Select * from Accounts where Id='" + userid + "';", sc);
                DataTable userdata = new DataTable();
                sqlusr.Fill(userdata);
                imie.Text = userdata.Rows[0][1].ToString();
                nazwisko.Text = userdata.Rows[0][2].ToString();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            /**
 *  Tworzy nową rezerwację na podstawie wyświetlanego lotu i danych pobranych z pól typu TextBox
 */
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqldspl = new SqlDataAdapter();
            sqldspl.SelectCommand = new SqlCommand("Select * from Flaights where Id='" + rezid + "';", sc);
            DataTable dt = new DataTable();
            sqldspl.Fill(dt);
            int mt = Convert.ToInt32(miejsca.Text);
            int dtv = Convert.ToInt32(dt.Rows[0][3]);
            int miejsca1 = dtv - mt;
            if (miejsca1 < 0)
            {
                MessageBox.Show("Nie ma tylu wolnych miejsc dla wybranego lotu");
            }
            else
            {
                SqlCommand sqlubt = new SqlCommand("Update Flaights set miejsca =" + miejsca1 + " where Id =" + rezid + "", sc);
                DataTable dt4 = new DataTable();
                SqlDataReader dr2;
                sc.Open();
                dr2 = sqlubt.ExecuteReader();
                while (dr2.Read())
                {

                }
                sc.Close();
                SqlDataAdapter sqlcheck = new SqlDataAdapter();
                sqlcheck.SelectCommand = new SqlCommand("Select Count(*) from Flaights where Id='" + rezid + "' and miejsca='" + miejsca1 + "';", sc);
                DataTable dt5 = new DataTable();
                sqlcheck.Fill(dt5);
                if (dt5.Rows[0][0].ToString() == "1")
                {
                    sqldspl.SelectCommand = new SqlCommand("Select * from Flaights where Id='" + rezid + "';", sc);
                    DataTable dg = new DataTable();
                    sqldspl.Fill(dg);
                    int cena = Convert.ToInt32(dg.Rows[0][4].ToString()) * Convert.ToInt32(miejsca.Text);
                    SqlCommand sqladd = new SqlCommand("Insert into rezerwations(imie, nazwisko, numerlotu, iloscmiejsc, koszt) values ('" + imie.Text + "','" + nazwisko.Text + "','" + rezid + "','" + miejsca.Text + "','" + cena + "');", sc);
                    SqlDataReader dr;
                    SqlConnection con = new SqlConnection();
                    sc.Open();
                    dr = sqladd.ExecuteReader();
                    while (dr.Read())
                    {

                    }
                    sc.Close();
                    SqlDataAdapter sqlchek = new SqlDataAdapter();
                    sqlchek.SelectCommand = new SqlCommand("Select Count(*) from rezerwations where imie ='" + imie.Text + "' and nazwisko ='" + nazwisko.Text + "' and numerlotu ='" + rezid + "' and iloscmiejsc ='" + miejsca.Text + "' and koszt='" + cena + "';", sc);
                    DataTable dt3 = new DataTable();
                    sqlchek.Fill(dt3);
                    if (dt3.Rows[0][0].ToString() == "1")
                    {
                        MessageBox.Show("Pomyślnie dokonano rezerwacji");
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Nie udało się dokonać rezerwacji");
                    }
                }
            }

        }
    }
}
