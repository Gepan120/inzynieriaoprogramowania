﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace UsługiLotnicze
{

    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        private void accountsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.accountsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.airflyDataSet);

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            /**
 * Przycisk odpowiadający za logowanie. Sprawdza wystąpienie takiej kombinacji loginu i hasła w bazie i jeśli jest, to weryfikuje jej poziom dostępu.
 */
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqlda = new SqlDataAdapter();
            sqlda.SelectCommand = new SqlCommand("Select Count(*) from Accounts where login ='" + loginTextBox.Text + "'and password ='" + passwordTextBox.Text + "'", sc);
            DataTable dt = new DataTable();
            sqlda.Fill(dt);
            if (dt.Rows[0][0].ToString() == "1")
            {

                sqlda.SelectCommand = new SqlCommand("Select * from Accounts where login ='" + loginTextBox.Text + "'and password ='" + passwordTextBox.Text + "'", sc);
                DataTable dt2 = new DataTable();
                sqlda.Fill(dt2);
                if (dt2.Rows[0][6].ToString() == "1")
                {
                    MessageBox.Show("Jesteś adminem");
                    var formm1 = Form1.ActiveForm;
                    formm1.Hide();
                    var admin = new Form5();
                    admin.Show();
                    this.Close();


                }
                else
                {
                    MessageBox.Show("Witaj użytkowniku");
                    int id = Convert.ToInt32(dt2.Rows[0][0].ToString());
                    var m = new Form6(id);
                    m.Show();
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Sprawdź swoje dane");
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            /**
 * Anuluje operacje logowania i powraca do głownego formularza
 */
            var m = new Form1();
            m.Show();
            this.Close();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

    }
}