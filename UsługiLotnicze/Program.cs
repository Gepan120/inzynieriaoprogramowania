﻿using System;
using System.Windows.Forms;

namespace UsługiLotnicze
{
    static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
        public static void Close()
        {
            if (MessageBox.Show("Czy na pewno chcesz zamknąć aplikację?", "Zamykanie", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {

            }
        }

    }
}

