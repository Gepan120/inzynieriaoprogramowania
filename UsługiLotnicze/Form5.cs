﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace UsługiLotnicze
{
    public partial class Form5 : Form
    {
        public string dbname;
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }
        private void Form5_FormClosed(Object sender, FormClosedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            /**
 * Przycisk pozwalający się wylogować i powrócić do pierwszego formularza.
 */
            if (MessageBox.Show("Czy na pewno chcesz się wylogować?", "Wylogowywanie", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var m = new Form1();
                m.Show();
                this.Hide();
            }
            else
            {

            }
        }

        private void Form5_FormClosed_1(object sender, FormClosedEventArgs e)
        {
            Form1.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /**
 * Przycisk pozwalający utworzyć nowe konto administratora (przekazanie parametru 1 w konstruktorze)
 */
            var m = new Form3(1);
            m.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            /**
 * Przycisk pozwalający wyświetlać dane z tabeli lotów
 */
            dbname = "Flaights";
            show(dbname);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /**
 * Przycisk odpowiedzialny za wyświetlanie danych z tabeli rezerwacji
 */
            dbname = "rezerwations";
            show(dbname);
        }
        void show(string dbname)
        {
            /**
 * Skrypt odpowiada za wyświetlanie danych z różnych tabel w zależności od parametru dbname
 */
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqldspl = new SqlDataAdapter();
            if (dbname == "Flaights")
            {
                sqldspl.SelectCommand = new SqlCommand("Select * from " + dbname + "", sc);
                DataTable dg = new DataTable();
                sqldspl.Fill(dg);
                dataGridView1.DataSource = dg;
            }
            else if (dbname == "rezerwations")
            {
                sqldspl.SelectCommand = new SqlCommand("Select * from " + dbname + "", sc);
                DataTable dg = new DataTable();
                sqldspl.Fill(dg);
                dataGridView1.DataSource = dg;
            }
            else
            {
                MessageBox.Show("Coś poszło nie tak, dzwoń na Helpdesk");
            }
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            /**
 * Skrypt odpowiedzialny za komunikację z bazą danych i dokonywnie zmian oraz dodawanie nowych rekordów do wybranej tabeli (parametr dbname)
 */
            if (dbname == "Flaights")
            {
                if (dataGridView1.CurrentRow != null)
                {
                    using (SqlConnection sqlCon = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;"))
                    {
                        sqlCon.Open();
                        DataGridViewRow dgvrow = dataGridView1.CurrentRow;
                        SqlCommand sqlcmd = new SqlCommand("FlaightsAddorEddit", sqlCon);
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        if (dgvrow.Cells["Id"].Value == DBNull.Value)
                        {
                            sqlcmd.Parameters.AddWithValue("@FlaightID", 0);
                        }
                        else
                        {
                            sqlcmd.Parameters.AddWithValue("@FlaightID", Convert.ToInt32((dgvrow.Cells["Id"].Value)));
                        }
                        sqlcmd.Parameters.AddWithValue("@Startingplace", dataGridView1.CurrentRow.Cells["startingplace"].Value == DBNull.Value ? "" : dataGridView1.CurrentRow.Cells["startingplace"].Value.ToString());
                        sqlcmd.Parameters.AddWithValue("@Destination", dataGridView1.CurrentRow.Cells["destination"].Value == DBNull.Value ? "" : dataGridView1.CurrentRow.Cells["destination"].Value.ToString());
                        sqlcmd.Parameters.AddWithValue("@miejsca", Convert.ToInt32(dataGridView1.CurrentRow.Cells["miejsca"].Value == DBNull.Value ? "0" : dataGridView1.CurrentRow.Cells["miejsca"].Value.ToString()));
                        sqlcmd.Parameters.AddWithValue("@cena", Convert.ToInt32(dataGridView1.CurrentRow.Cells["cena"].Value == DBNull.Value ? "0" : dataGridView1.CurrentRow.Cells["cena"].Value.ToString()));
                        sqlcmd.Parameters.AddWithValue("@dataodlotu", dataGridView1.CurrentRow.Cells["Data odlotu"].Value == DBNull.Value ? "" : dataGridView1.CurrentRow.Cells["Data odlotu"].Value.ToString());
                        sqlcmd.ExecuteNonQuery();
                        show(dbname);
                    }
                }
            }
            else if (dbname == "rezerwations")
            {
                if (dataGridView1.CurrentRow != null)
                {
                    using (SqlConnection sqlCon = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;"))
                    {
                        sqlCon.Open();
                        DataGridViewRow dgvrow = dataGridView1.CurrentRow;
                        SqlCommand sqlcmd = new SqlCommand("rezerwationsEdditorAdd", sqlCon);
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        if (dgvrow.Cells["id"].Value == DBNull.Value)
                        {
                            sqlcmd.Parameters.AddWithValue("@rezerwationID", 0);
                        }
                        else
                        {
                            sqlcmd.Parameters.AddWithValue("@rezerwationID", Convert.ToInt32((dgvrow.Cells["id"].Value)));
                        }
                        sqlcmd.Parameters.AddWithValue("@imie", dataGridView1.CurrentRow.Cells["Imie"].Value == DBNull.Value ? "" : dataGridView1.CurrentRow.Cells["Imie"].Value.ToString());
                        sqlcmd.Parameters.AddWithValue("@nazwisko", dataGridView1.CurrentRow.Cells["Nazwisko"].Value == DBNull.Value ? "" : dataGridView1.CurrentRow.Cells["Nazwisko"].Value.ToString());
                        sqlcmd.Parameters.AddWithValue("@numerlotu", Convert.ToInt32(dataGridView1.CurrentRow.Cells["numerlotu"].Value == DBNull.Value ? "0" : dataGridView1.CurrentRow.Cells["numerlotu"].Value.ToString()));
                        sqlcmd.Parameters.AddWithValue("@iloscmiejsc", Convert.ToInt32(dataGridView1.CurrentRow.Cells["iloscmiejsc"].Value == DBNull.Value ? "0" : dataGridView1.CurrentRow.Cells["iloscmiejsc"].Value.ToString()));
                        sqlcmd.Parameters.AddWithValue("@koszt", Convert.ToInt32(dataGridView1.CurrentRow.Cells["koszt"].Value == DBNull.Value ? "0" : dataGridView1.CurrentRow.Cells["koszt"].Value.ToString()));
                        sqlcmd.ExecuteNonQuery();
                        show(dbname);
                    }
                }
            }
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            /**
 * Skrypt odpowiedzialny za usuwanie wybranych rekordów z wybranej tabeli
 */
            if (dbname == "Flaights")
            {
                if (dataGridView1.CurrentRow.Cells["Id"].Value != DBNull.Value)
                {
                    if (MessageBox.Show("Czy na pewno chcesz usunąć ten lot?", "DataGridView", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        using (SqlConnection sqlCon = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;"))
                        {
                            sqlCon.Open();
                            SqlCommand sqlCmd = new SqlCommand("FlaightsDelate", sqlCon);
                            sqlCmd.CommandType = CommandType.StoredProcedure;
                            sqlCmd.Parameters.AddWithValue("@FlaightID", Convert.ToInt32((dataGridView1.CurrentRow.Cells["Id"].Value)));
                            sqlCmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else if (dbname == "rezerwations")
            {
                if (dataGridView1.CurrentRow.Cells["id"].Value != DBNull.Value)
                {
                    if (MessageBox.Show("Czy na pewno chcesz usunąć tę rezerwacje?", "DataGridView", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        using (SqlConnection sqlCon = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;"))
                        {
                            sqlCon.Open();
                            SqlCommand sqlCmd = new SqlCommand("rezerwationdelete", sqlCon);
                            sqlCmd.CommandType = CommandType.StoredProcedure;
                            sqlCmd.Parameters.AddWithValue("@rezerwationID", Convert.ToInt32((dataGridView1.CurrentRow.Cells["id"].Value)));
                            sqlCmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
