﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace UsługiLotnicze
{
    public partial class Form1 : Form
    {
        public static string IdValue;
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /**
 * Ładowanie danych do tabeli następujące przy wczytywaniu formularzu
 */
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqldspl = new SqlDataAdapter();
            sqldspl.SelectCommand = new SqlCommand("Select * from Flaights", sc);
            DataTable dg = new DataTable();
            sqldspl.Fill(dg);
            flaightsDataGridView.DataSource = dg;

        }
        private void Form5_Load(object sender, System.EventArgs e)
        {
            this.Hide();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            /**
 *  Przyciks otwierający formularz rejestracji nowego użytkownika = parametr 0
 */
            var m = new Form3(0);
            m.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            /**
 * Przycisk otwierający okno logowania
 */
            var m = new Form2();
            m.Show();
            this.Hide();
        }

        private void flaightsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /**
 * W momencie wybrania komórki w bazie danych otwiera się okno rezerwacji lotu z nią skojarzonego 
 */
            var rowIndex = e.RowIndex;
            var currentRow = flaightsDataGridView.Rows[rowIndex];
            var value = Convert.ToInt32(flaightsDataGridView.Rows[rowIndex].Cells[0].Value.ToString());
            var m = new Form4(value, 0);
            m.Show();
        }

        private void sptextbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void destextbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void datetextbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void serchbutton_Click(object sender, EventArgs e)
        {
            /**
 * Przycisk odpowiadający za wyszukiwanie i wyświetlanie danych w tabeli Lotów na podstawie kryteriów wpisanych w oknach textbox
 */
            string theDate = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqlquery = new SqlDataAdapter();
            sqlquery.SelectCommand = new SqlCommand("Select * from Flaights where startingplace like '" + sptextbox.Text + "%' and destination like'" + destextbox.Text + "%' and [Data odlotu] >'" + theDate + "' ;", sc);
            DataTable dg = new DataTable();
            sqlquery.Fill(dg);
            flaightsDataGridView.DataSource = dg;

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }
        private void flaightsDataGridView_CellClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {

        }

        private void flaightsDataGridView_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void serchbutton_Click_1(object sender, EventArgs e)
        {
            /**
 *  Przycisk odpowiadający za wyszukiwanie i wyświetlanie danych w tabeli Lotów na podstawie kryteriów wpisanych w oknach textbox
 */
            string theDate = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqlquery = new SqlDataAdapter();
            sqlquery.SelectCommand = new SqlCommand("Select * from Flaights where startingplace like '" + sptextbox.Text + "%' and destination like'" + destextbox.Text + "%' and [Data odlotu] >'" + theDate + "' ;", sc);
            DataTable dg = new DataTable();
            sqlquery.Fill(dg);
            flaightsDataGridView.DataSource = dg;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
        }
        public static void Close()
        {
            /**
 * Odpowiada za wyskoczenie okna pytającego się czy na pewno chcemy zamknąć program.
 */
            Program.Close();
        }
    }
}
