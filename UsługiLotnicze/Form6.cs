﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace UsługiLotnicze
{
    public partial class Form6 : Form
    {
        public int userid;
        public string dbname;
        public DataTable user = new DataTable();
        public Form6(int id)
        {
            InitializeComponent();
            userid = id;
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            /**
 * Inicjacja formularza pobiera dane przekazane jej przez konstruktor na temat ID użytkownika by wyświetlić jego imię i nazwisko w lewym rogu ekranu
 */
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqlda = new SqlDataAdapter();
            sqlda.SelectCommand = new SqlCommand("Select * from Accounts where id ='" + userid + "';", sc);
            sqlda.Fill(user);
            label1.Text = "Witaj" + user.Rows[0][1].ToString() + " " + user.Rows[0][2].ToString() + "!";
            show("Flaights");
        }

        private void label1_Enter(object sender, EventArgs e)
        {

        }

        private void Form6_FormClosed(object sender, FormClosedEventArgs e)
        {
            /**
 * Zamykanie programu przy zamknięciu tego okna
 */
            Form1.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /**
 * Wylogowywanie użytkownika i powrót do głównego okna dla niezalogowanych użytkowników
 */
            if (MessageBox.Show("Czy na pewno chcesz się wylogować?", "Wylogowywanie", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var m = new Form1();
                m.Show();
                this.Hide();
            }
            else
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /**
 * Przycisk odpowiadający za wybór tabeli "loty"
 */
            dbname = "Flaights";
            show(dbname);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            /**
 * Przycisk wybierający tabelę "rzezerwacje" i pozwalający użytkownikowi przeglądać jego rezerwacje
 */
            dbname = "rezerwations";
            show(dbname);
        }
        void show(string dbname)
        {
            /**
 * Funkcja odpowiedzialna za wyświetlanie określonej tabeli i chowanie/wyświetlanie przycisków do wyszukiwania dla tabeli "Loty"
 */
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqldspl = new SqlDataAdapter();
            if (dbname == "Flaights")
            {
                sqldspl.SelectCommand = new SqlCommand("Select * from " + dbname + "", sc);
                DataTable dg = new DataTable();
                sqldspl.Fill(dg);
                dataGridView1.DataSource = dg;
                label2.Show();
                label3.Show();
                label4.Show();
                textBox1.Show();
                textBox2.Show();
                dateTimePicker1.Show();
                button4.Show();

            }
            else if (dbname == "rezerwations")
            {
                sqldspl.SelectCommand = new SqlCommand("Select * from " + dbname + " where Imie like '" + user.Rows[0][1].ToString() + "' and Nazwisko like '" + user.Rows[0][2].ToString() + "';", sc);
                DataTable dg = new DataTable();
                sqldspl.Fill(dg);
                dataGridView1.DataSource = dg;
                label2.Hide();
                label3.Hide();
                label4.Hide();
                textBox1.Hide();
                textBox2.Hide();
                dateTimePicker1.Hide();
                button4.Hide();
            }
            else
            {
                MessageBox.Show("Coś poszło nie tak, dzwoń na Helpdesk");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /**
 * Przycisk wyszukiwania lotów na podstawie danych określonych w oknach TextBox
 */
            string theDate = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqlquery = new SqlDataAdapter();
            sqlquery.SelectCommand = new SqlCommand("Select * from Flaights where startingplace like '" + textBox1.Text + "%' and destination like'" + textBox2.Text + "%' and [Data odlotu] >'" + theDate + "' ;", sc);
            DataTable dg = new DataTable();
            sqlquery.Fill(dg);
            dataGridView1.DataSource = dg;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /**
 * Funkcja umożliwiająca rezerwację danego lotu po wybraniu go z tabeli danych
 */
            var rowIndex = e.RowIndex;
            var currentRow = dataGridView1.Rows[rowIndex];
            var value = Convert.ToInt32(dataGridView1.Rows[rowIndex].Cells[0].Value.ToString());
            var m = new Form4(value, userid);
            m.Show();
        }
    }
}
