using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UsługiLotnicze
{
    public partial class Form5 : Form
    {
        public string dbname;
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }
        private void Form5_FormClosed(Object sender, FormClosedEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Wylogowywanie");
            var m = new Form1();
            m.Show();
            this.Hide();

        }

        private void Form5_FormClosed_1(object sender, FormClosedEventArgs e)
        {
            MessageBox.Show("Zamykamy");
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var m = new Form3(1);
            m.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dbname = "Flaights";
            show(dbname);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dbname = "rezerwations";
            show(dbname);
        }
        void show(string dbname)
        {
            SqlConnection sc = new SqlConnection("Data Source = .\\SQLEXPRESS; Initial Catalog = Airfly; Integrated Security = True;");
            SqlDataAdapter sqldspl = new SqlDataAdapter();
            if (dbname == "Flaights")
            {
                sqldspl.SelectCommand = new SqlCommand("Select startingplace, destination, miejsca, cena, [Data odlotu] from " + dbname + "", sc);
                DataTable dg = new DataTable();
                sqldspl.Fill(dg);
                dataGridView1.DataSource = dg;
            }
            else if (dbname == "rezerwations")
            {
                sqldspl.SelectCommand = new SqlCommand("Select Imie, Nazwisko, numerlotu, iloscmiejsc, koszt from " + dbname + "", sc);
                DataTable dg = new DataTable();
                sqldspl.Fill(dg);
                dataGridView1.DataSource = dg;
            }
            else
            {
                MessageBox.Show("Coś poszło nie tak, dzwoń na Helpdesk");
            }
        }
    }
    }
